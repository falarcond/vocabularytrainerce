var lenghtCurrentData = 0;
var localStorageBackground = 'backgroundJs';
var localStorageAlarmInProgress = 'alarmInProgress';

chrome.runtime.onInstalled.addListener(function () {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
            })
            ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
    chrome.browserAction.setBadgeBackgroundColor({ color: [255, 0, 0, 255] });
    chrome.storage.local.clear(function() {
        var error = chrome.runtime.lastError;
        if (error) {
            console.error(error);
        }
    });
});

chrome.notifications.onClosed.addListener(function (alarmName) {
    chrome.notifications.clear(alarmName);
});

chrome.alarms.onAlarm.addListener((alarm) => {
    chrome.alarms.clear(alarm.name);
    if (alarm.name === 'timeOutToChangeWord') {
        chrome.notifications.clear('vocabularyTrainerNotification', () => {
            chrome.browserAction.setBadgeText({ text: "1" });
            chrome.notifications.create(
                'vocabularyTrainerNotification', {
                    type: 'basic',
                    iconUrl: 'images/img48.png',
                    title: "Hey there!",
                    message: "There is a new word!",
                    requireInteraction: true
                }, () => { }
            );
        });

        chrome.storage.sync.get(localStorageBackground, (result) => {
            console.log('get ' + localStorageBackground + ' from background.js');
            result[localStorageBackground] = !result[localStorageBackground];
            console.log(result);
            chrome.storage.sync.set(result);

            var objAlarInProgress = {};
            objAlarInProgress[localStorageAlarmInProgress] = false;
            chrome.storage.sync.set(objAlarInProgress);
        });
    }
});
