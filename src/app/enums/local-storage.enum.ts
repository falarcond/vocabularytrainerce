export enum LocalStorageEnum {
    ItemSelected = 'itemSelected',
    Data = 'vocabulary',
    Auth = 'authentication',
    Token = 'token',
    BackgroundJs = 'backgroundJs',
    AlarmInProgress = 'alarmInProgress'
}
