import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { GoogleOauth2Service } from './google-api/google-oauth2.service';
import { Observable, Subject } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import { LocalStorageEnum } from '../enums/local-storage.enum';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {
  constructor(private googleOauth2Service: GoogleOauth2Service,
    private localStorageService: LocalStorageService,
    private router: Router) { }

  canActivate(): Observable<boolean> {
    const result$ = new Subject<boolean>();
    this.googleOauth2Service.isAuthenticated().subscribe((value) => {
      console.log('isAuthenticated: ' + value);
      if (!value) {
        this.localStorageService.setItemByName(LocalStorageEnum.Token, '').subscribe(() => {
          this.router.navigate(['login']);
        });
      }
      return result$.next(!!value);
    });

    return result$.asObservable();
  }
}
