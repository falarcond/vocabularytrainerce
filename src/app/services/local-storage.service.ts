import { Injectable, EventEmitter } from '@angular/core';
import { DataInfo } from '../models/data-info.model';
import { Observable, Subject } from 'rxjs';
import { LocalStorageEnum } from '../enums/local-storage.enum';
import { CategoryEnum } from '../enums/category.enum';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  public triggerItem = new EventEmitter<LocalStorageEnum>();
  private readonly chrome: any;

  constructor() {
    this.chrome = window['chrome'];
    this.itemChangedEvent();
  }

  public getItemByName<T>(name: LocalStorageEnum): Observable<T> {
    const result$ = new Subject<T>();

    this.chrome.storage.sync.get(name, (result: {[key: string]: T}) => {
      result$.next(result[name]);
    });

    return result$.asObservable();
  }

  public setItemByName(name: LocalStorageEnum, value: string): Observable<void> {
    const result$ = new Subject<void>();

    const item = {};
    item[name] = value;
    window.localStorage.setItem(name, value);
    this.chrome.storage.sync.set(item, () => {
      result$.next();
    });

    return result$.asObservable();
  }

  public getAllData(withoutFilter?: boolean): Observable<DataInfo[]> {
    const result$ = new Subject<DataInfo[]>();

    this.getItemByName<DataInfo[]>(LocalStorageEnum.Data).subscribe(result => {
      if (result) {
        let data: DataInfo[] = JSON.parse(<string><Object>result);
        data = data.filter((dataInfo) => {
          return (!!withoutFilter) ? true : !dataInfo.Updated;
        });
        result$.next(data);
      } else {
        result$.next([]);
      }
    });

    return result$.asObservable();
  }

  public getItemSelected(): Observable<DataInfo> {
    console.log('getItemData');
    const result$ = new Subject<DataInfo>();

    this.getAllData(true).subscribe((data) => {
      this.getItemByName<number>(LocalStorageEnum.ItemSelected).subscribe((itemSelected) => {
        if (!!data[itemSelected]) {
          console.log('getItemData.getItem.true');
          result$.next(data[itemSelected]);
        } else {
          console.log('getItemData.getItem.false');
          const dataEmpty: DataInfo = {
            Index: 0,
            Word: '',
            Context: '',
            Translation: '',
            Failures: 0,
            Category: CategoryEnum.A,
            Timestamp: '',
            Updated: ''
          };
          result$.next(dataEmpty);
        }
      });
    });

    return result$.asObservable();
  }

  public updateItemData(itemDataInfoToUpdate: DataInfo): Observable<any> {
    const result$ = new Subject<any>();

    console.log('updateItemData');
    this.getAllData(true).subscribe((data) => {
      this.getItemByName<number>(LocalStorageEnum.ItemSelected).subscribe((index) => {
        data[index] = itemDataInfoToUpdate;
        this.setItemByName(LocalStorageEnum.Data, JSON.stringify(data));
        this.setItemByName(LocalStorageEnum.ItemSelected, '-1');
        result$.next();
      });
    });

    return result$.asObservable();
  }

  private itemChangedEvent(): void {
    this.chrome.storage.onChanged.addListener((changes) => {
      console.log('storage.onChanged');
      for (const key in changes) {
        if (changes.hasOwnProperty(key)) {
          this.triggerItem.emit(<LocalStorageEnum>key);
        }
      }
    });
  }
}
