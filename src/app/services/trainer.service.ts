import { Injectable, EventEmitter } from '@angular/core';
import { GoogleSheetService } from './google-api/google-sheet.service';
import { DataInfo } from '../models/data-info.model';
import { LocalStorageService } from './local-storage.service';
import { AlarmService } from './alarm.service';
import { Observable, Subject } from 'rxjs';
import { LocalStorageEnum } from '../enums/local-storage.enum';
import { CategoryEnum } from '../enums/category.enum';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {
  public trigger = new EventEmitter<DataInfo>();
  public currentDataInfo: DataInfo;
  private readonly TIME_OUT = 5 * 60000;

  constructor(private googleSheetService: GoogleSheetService,
    private localStorageService: LocalStorageService,
    private alarmService: AlarmService) {
      this.initTriggerLocalStorage();
      this.initLoadData();
  }

  set setTrigger(value: DataInfo) {
    this.currentDataInfo = value;
    this.trigger.emit(value);
  }

  public getNewWord(failure: boolean) {
    console.log('getNewWord');
    const currentDataInfo = this.currentDataInfo;
    currentDataInfo.Updated = `${Math.round(Date.now() / 1000)}`;

    if (currentDataInfo.Failures > 0) {
      currentDataInfo.Failures += failure ? 1 : -1;
    } else if (+currentDataInfo.Failures === 0 && failure) {
      currentDataInfo.Failures++;
    }
    this.localStorageService.updateItemData(currentDataInfo).subscribe(() => {
      this.alarmService.setTimeOut(this.TIME_OUT);
    });
  }

  private initLoadData() {
    console.log('init');
    this.localStorageService.getItemByName<number>(LocalStorageEnum.ItemSelected).subscribe((itemSelected) => {
      this.localStorageService.getItemByName<boolean>(LocalStorageEnum.AlarmInProgress).subscribe((result) => {
        if (+itemSelected === -1 && !result) {
          console.log('hasLocalStorage');
          this.renewItem();
        } else if (typeof(JSON.parse(<any>itemSelected || null)) !== 'number') {
          console.log('no hasLocalStorage');
          this.getDataFromGoogle();
        } else {
          this.localStorageService.getItemSelected().subscribe((value) => {
            this.setTrigger = value;
          });
        }
      });
    });
  }

  private getDataFromGoogle(): void {
    this.getData().subscribe((googleData) => {
      const originalData = googleData['values'];
      const dataInfo: DataInfo[] = originalData.map((item: Object) => {
        return this.mapper(item);
      });
      dataInfo.shift();
      this.localStorageService.setItemByName(LocalStorageEnum.Data, JSON.stringify(dataInfo)).subscribe(() => {
        this.renewItem();
      });
    });
  }

  private getData(): Observable<Object> {
    const result$ = new Subject<Object>();

    this.googleSheetService.getDriveFile().subscribe((spreadsheetId) => {
      this.googleSheetService.getSheetFile(spreadsheetId, 'Query').subscribe((data) => {
        result$.next(data);
      });
    });

    return result$.asObservable();
  }

  private renewItem(): void {
    this.localStorageService.getAllData().subscribe((activeData) => {
      const activeDataLength = activeData.length;

      if (activeDataLength > 0) {
        const randomIndex = this.getRandomItem(activeDataLength - 1);
        const dataInfo: DataInfo = activeData[randomIndex];
        this.localStorageService.getAllData(true).subscribe((data) => {
          const newIndex = data.findIndex((item) => item.Index === dataInfo.Index);
          this.localStorageService.setItemByName(LocalStorageEnum.ItemSelected, `${newIndex}`);
        });
      } else {
        this.getDataFromGoogle();
      }
    });
  }

  private getRandomItem(max: number): number { // return from 0 to max
    return Math.floor(Math.random() * (max + 1));
  }

  private mapper(value: Object): DataInfo {
    const result: DataInfo = {
      Index: +value[0] || 0,
      Word: value[1] || '',
      Context: value[2] || '',
      Translation: value[3] || '',
      Failures: +value[4] || 0,
      Category: value[5] || CategoryEnum.A,
      Timestamp: value[6] || '',
      Updated: value[7] || ''
    };

    return result;
  }

  private initTriggerLocalStorage() {
    this.localStorageService.triggerItem.subscribe((localStorageEnum: LocalStorageEnum) => {
      console.log(localStorageEnum);
      switch (localStorageEnum) {
        case LocalStorageEnum.ItemSelected:
          this.localStorageService.getItemSelected().subscribe((value) => {
            this.setTrigger = value;
          });
          break;
        case LocalStorageEnum.BackgroundJs:
          this.renewItem();
          break;
      }
    });
  }
}
