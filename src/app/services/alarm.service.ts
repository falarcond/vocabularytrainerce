import { Injectable } from '@angular/core';
import { Alarm } from '../models/alarm.model';
import { Observable, Subject } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import { LocalStorageEnum } from '../enums/local-storage.enum';

@Injectable({
  providedIn: 'root'
})
export class AlarmService {
  private readonly chrome: any;
  private readonly ALARM_NAME = 'timeOutToChangeWord';

  constructor(private localStorageService: LocalStorageService) {
    this.chrome = window['chrome'];
  }

  private checkAlarm(): Observable<boolean> {
    const result$ = new Subject<boolean>();

    this.chrome.alarms.get((alarm: Alarm) => {
      const result = !!alarm ? true : false;
      result$.next(result);
    });

    return result$.asObservable();
  }

  public create(name: string, time: number) {
    this.chrome.alarms.create(name, {
      when: Date.now() + time
    });
    this.localStorageService.setItemByName(LocalStorageEnum.AlarmInProgress, 'true');
  }

  public setTimeOut(time?: number) {
    this.chrome.browserAction.setBadgeText({text: ''});
    console.log('setTimeOut');
    this.checkAlarm().subscribe((value) => {
      console.log('checkAlarm');
      if (!value) {
        const timeOut = time || 10000;
        this.localStorageService.getAllData().subscribe((data) => {
          const alarmName = this.ALARM_NAME;
          this.create(alarmName, timeOut);
          setTimeout(() => {
            window.close();
          }, 50);
        });
      }
    });
  }
}
