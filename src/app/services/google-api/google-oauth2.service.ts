import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { LocalStorageService } from '../local-storage.service';
import { LocalStorageEnum } from 'src/app/enums/local-storage.enum';

@Injectable({
  providedIn: 'root'
})
export class GoogleOauth2Service {
  private readonly chrome: any;

  constructor(private localStorageService: LocalStorageService) {
    this.chrome = window['chrome'];
  }

  public isAuthenticated(): Observable<boolean> {
    return this.localStorageService.getItemByName<boolean>(LocalStorageEnum.Auth);
  }

  openOauth2(): Observable<string> {
    const result$ = new Subject<string>();

    this.chrome.identity.getAuthToken({ interactive: true }, (token: string) => {
      console.log('openOauth2:' + token);
      if (!!token) {
        result$.next(token);
      } else {
        result$.error('');
      }
    });

    return result$.asObservable();
  }

  renewOauth2(token: string) {
    this.removeOauth2(token).subscribe(() => {
      console.log('renewOauth2');
      this.openOauth2();
    });
  }

  removeOauth2(token: string): Observable<void> {
    const result$ = new Subject<void>();

    this.chrome.identity.removeCachedAuthToken({ token: token }, () => {
      //save vocabulary data
      //remove vocabulary data
      result$.next();
    });

    return result$.asObservable();
  }

  public getToken(): Observable<string> {
    return this.localStorageService.getItemByName<string>(LocalStorageEnum.Token);
  }
}

export enum EndPoints {
  DriveFileList = 'https://www.googleapis.com/drive/v3/files',
  ValuesGet = 'https://sheets.googleapis.com/v4/spreadsheets/{spreadsheetId}/values/{range}',
  SpreadsheetGet = 'https://sheets.googleapis.com/v4/spreadsheets/{spreadsheetId}',
  SpreadsheetDelete = 'https://sheets.googleapis.com/v4/spreadsheets/11ENgwRBoMCUIgbUcdpjyHbp8HYEk8gwZREs3ZU8nMnQ:batchUpdate'
}
