import { TestBed } from '@angular/core/testing';

import { GoogleOauth2Service } from './google-oauth2.service';

describe('GoogleOauth2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GoogleOauth2Service = TestBed.get(GoogleOauth2Service);
    expect(service).toBeTruthy();
  });
});
