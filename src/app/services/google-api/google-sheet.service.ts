import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GoogleOauth2Service, EndPoints } from './google-oauth2.service';
import { LocalStorageService } from '../local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class GoogleSheetService {
  private readonly URL_READ_SHEET: string = 'assets/sample.json';

  constructor(private http: HttpClient,
    private googleOauth2Service: GoogleOauth2Service,
    private localStorageService: LocalStorageService) { }

  getData(): Observable<Object> {
    return this.http.get<Object>(this.URL_READ_SHEET);
  }

  private updateSheets(tabs: { [title: string]: number }[]): Observable<boolean> {
    const result$ = new Subject<boolean>();
    const idTabsAtoDcategories = tabs.filter((item) => {
      return ['A', 'B', 'C', 'D', 'E'].some((category) => {
        return category === Object.keys(item)[0];
      });
    });
    if (idTabsAtoDcategories.length > 0) {
      this.googleOauth2Service.getToken().subscribe((token) => {
        const options = {
          headers: {
            Authorization: 'Bearer ' + token,
            'Content-Type': 'application/json'
          }
        };

        idTabsAtoDcategories.forEach((element, index, arr) => {
          this.http.get<Object>(EndPoints.SpreadsheetDelete, options).subscribe(() => {
            if (arr.length - 1 === index) { // last item
              result$.next(true);
            }
          }, (error) => {
            result$.error(error);
          });
        });
      });
    } else {
      result$.next(true);
    }

    return result$.asObservable();
  }

  public getDriveFile(): Observable<string> {
    const result$ = new Subject<string>();

    this.googleOauth2Service.getToken().subscribe((token) => {
      const options = {
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json'
        }
      };
      const queryParameters = encodeURI('?q=name="VocabularyTrainer"');
      this.http.get<Object>(EndPoints.DriveFileList + queryParameters, options).subscribe((data) => {
        const idDriveFile = data['files'][0].id;
        result$.next(idDriveFile);
      }, (error) => {
        result$.error(error);
      });
    });

    return result$.asObservable();
  }

  public getSheetFile(spreadsheetId: string, tabName: string): Observable<Object> { // modify range to query tab
    const result$ = new Subject<Object>();

    this.googleOauth2Service.getToken().subscribe((token) => {
      const options = {
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json'
        }
      };
      const urlSheet = EndPoints.ValuesGet.replace('{spreadsheetId}', spreadsheetId)
        .replace('{range}', encodeURI(tabName + '!A1:H2000?majorDimension=ROWS'));
      this.http.get<Object>(urlSheet, options).subscribe((data) => {
        result$.next(data);
      }, (error) => {
        result$.error(error);
      });
    });

    return result$.asObservable();
  }

  public getTabs(spreadsheetId: string): Observable<{ [title: string]: number }[]> {
    const result$ = new Subject<{ [title: string]: number }[]>();

    this.googleOauth2Service.getToken().subscribe((token) => {
      const options = {
        headers: {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'application/json'
        }
      };
      const urlTabOfSheet = EndPoints.SpreadsheetGet.replace('{spreadsheetId}', spreadsheetId);
      this.http.get<Object>(urlTabOfSheet, options).subscribe((data) => {
        const tabsId: Object[] = data['sheets'];
        const ids: { [title: string]: number }[] = tabsId.map((item) => {
          const itemDictionary: { [title: string]: number } = {};
          itemDictionary[`${item['properties'].title}`] = +item['properties'].sheetId;
          return itemDictionary;
        });
        console.log('tabs: ' + JSON.stringify(ids));
        result$.next(ids);
      });
    });

    return result$.asObservable();
  }

  //en getwords I should apply the last item of this list if there are list in inactive state
  //get total and calculate limit by each group
  //delete queries tab
  //select union all with limit by each group and order by failures desc and unixtime asc
  //get sheet ids then delete and append by each group (A to E)

  //new option to save in cloud (get queries data and compare with local if it is equal then update in cloud and get data else no update and get data)
  //new option to get last changes from cloud (get data cloud and overwrite local)
  //confirm reload next bank of words (get queries data and compare with local if it is equal then update in cloud and get data else no update and get data)
}
