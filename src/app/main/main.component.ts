import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { TrainerService } from '../services/trainer.service';
import { DataInfo } from '../models/data-info.model';
import { LocalStorageService } from '../services/local-storage.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  public data: DataInfo;
  public allData: DataInfo[];
  public translate = false;
  public restantWords: number;
  public totalWords: number;

  constructor(private trainerService: TrainerService, private zone: NgZone, private localStorageService: LocalStorageService) {
    this.loadData();
  }

  ngOnInit(): void {
    this.trainerService.trigger.subscribe((value: DataInfo) => {
      this.localStorageService.getAllData(true).subscribe((data) => {
        this.allData = data;
        this.totalWords = data.length;
        this.restantWords = data.filter((item) => !item.Updated).length;
        this.refreshTemplate();
      });

      this.data = value;
      this.refreshTemplate();
    });
  }

  public registerFailure(failure: boolean): void {
    console.log('registerFailure');
    this.trainerService.getNewWord(failure);
  }

  public toogleTranslate(): void {
    console.log('toogleTranslate');
    this.translate = !this.translate;
  }

  public getWord(): string {
    const result = (this.data.Index === 0) ? 'Waiting for new word...' : this.data.Word;
    return result;
  }

  public getContext(): string {
    let result = this.data.Context;

    if (this.translate) {
      result = this.data.Translation;
    }

    return result;
  }

  public isDebuggerMode(all: boolean): boolean {
    let result: boolean;
    if (all) {
      result = localStorage.getItem('debugAll') === 'true';
    } else {
      result = localStorage.getItem('debug') === 'true';
    }
    return result;
  }

  public openForm(): void {
    window.open('https://docs.google.com/spreadsheets/d/11ENgwRBoMCUIgbUcdpjyHbp8HYEk8gwZREs3ZU8nMnQ/edit', '_blank');
  }

  private refreshTemplate(): void {
    setTimeout(() => {
      this.zone.run(() => {});
    });
  }

  private loadData(): void {
    this.data = this.trainerService.currentDataInfo;
  }
}
