import { CategoryEnum } from '../enums/category.enum';

export class DataInfo {
    Index: number;
    Word: string;
    Context: string;
    Translation: string;
    Failures: number;
    Category: CategoryEnum;
    Timestamp: string;
    Updated: string;
}
