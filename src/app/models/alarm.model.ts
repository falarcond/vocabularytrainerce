export class Alarm {
    name: string;
    scheduledTime: number;
    periodInMinutes: number;
}
