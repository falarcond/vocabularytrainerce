import { Component } from '@angular/core';
import { GoogleOauth2Service } from '../services/google-api/google-oauth2.service';
import { LocalStorageService } from '../services/local-storage.service';
import { LocalStorageEnum } from '../enums/local-storage.enum';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(private googleOauth2Service: GoogleOauth2Service,
    private localStorageService: LocalStorageService) {
  }

  public login() {
    this.googleOauth2Service.openOauth2().subscribe((newToken: string) => {
      console.log(newToken);
      this.localStorageService.setItemByName(LocalStorageEnum.Token, newToken).subscribe(() => {
        this.localStorageService.setItemByName(LocalStorageEnum.Auth, 'true').subscribe(() => {
          window.close();
        });
      });
    });
  }
}
