#!/bin/bash

RED='\033[00;31m'
GREEN='\033[00;32m'
NORMAL=$(tput sgr0)

case $1 in
  "web")
    ng build --aot --prod --output-path dist-web/
  ;;
  "chrome")
    ng build --aot --prod --output-path dist/dist-chrome/
    rm -r dist/dist-chrome/*.ico dist/dist-chrome/*.txt dist/dist-chrome/assets
    cp -r src/assets/chrome_extension/* dist/dist-chrome/
    mkdir dist/dist-chrome/assets
    cp src/assets/sample.json dist/dist-chrome/assets
    sh replace-manifest.sh
    sh decode.sh
    printf "${GREEN}Chrome extension package was created!${NORMAL}"
  ;;
  *)
    printf "${RED}Invalid option!${NORMAL}"
  ;;
esac
