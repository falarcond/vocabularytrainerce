#!/bin/bash

GREEN='\033[00;32m'
NORMAL=$(tput sgr0)

cd dist/dist-chrome/

# RegExp to get js and css keys
RE_JS="(?<=\"js\":\s).*"
RE_CSS="(?<=\"css\":\s).*"

CURRENT_JS=$(perl -nle 'print $& if m{'$RE_JS'}' manifest.json)
CURRENT_JS="${CURRENT_JS//],*/''}"
CURRENT_JS="${CURRENT_JS//[/''}"
CURRENT_CSS=$(perl -nle 'print $& if m{'$RE_CSS'}' manifest.json)
CURRENT_CSS="${CURRENT_CSS//]*/''}"
CURRENT_CSS="${CURRENT_CSS//[/''}"

FILES="$(ls -I background.js | egrep '\.js$|\.css$')"
NEW_JS=""
NEW_CSS=""

for FILE in $FILES; do
    if [[ $FILE == *".js" ]]; then
        NEW_JS="$NEW_JS\"$FILE\","
    else
        NEW_CSS="$NEW_CSS\"$FILE\","
    fi
done
NEW_JS="${NEW_JS/%,/''}"
NEW_CSS="${NEW_CSS/%,/''}"

printf "${NORMAL}Current js is: ${GREEN} ${CURRENT_JS}. ${NORMAL}Will apply: ${GREEN}${NEW_JS}\n"
printf "${NORMAL}Current css is: ${GREEN} ${CURRENT_CSS}. ${NORMAL}Will apply: ${GREEN}${NEW_CSS}${NORMAL}"

# Replace the links in the manifest
sed -i 's/'$CURRENT_JS'/'$NEW_JS'/g' manifest.json
sed -i 's/'$CURRENT_CSS'/'$NEW_CSS'/g' manifest.json;
exit 0
