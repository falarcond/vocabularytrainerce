#!/bin/bash

RED='\033[00;31m'
GREEN='\033[00;32m'
NORMAL=$(tput sgr0)

cd dist/dist-chrome/

printf "${RED}\nEnter key >> ${NORMAL}"
read KEY

KEY_MD5=$(echo -n $KEY | md5sum | cut -d" " -f1)
KEY_HEX=$(echo -n $KEY_MD5 | od -A n -t x1 | sed ':a;N;$!ba;s/\n/ /g' | sed 's/ *//g')

# RegExp to get client_id and key
RE_KEY="(?<=\"key\":\s).*"
RE_CLIENT_ID="(?<=\"client_id\":\s).*"

CURRENT_KEY=$(perl -nle 'print $& if m{'$RE_KEY'}' manifest.json)
CURRENT_KEY="${CURRENT_KEY//,*/''}"
CURRENT_KEY="${CURRENT_KEY//\"/''}"

CURRENT_CLIENT_ID=$(perl -nle 'print $& if m{'$RE_CLIENT_ID'}' manifest.json)
CURRENT_CLIENT_ID="${CURRENT_CLIENT_ID//,*/''}"
CURRENT_CLIENT_ID="${CURRENT_CLIENT_ID//\"/''}"

ORIGINAL_KEY=$(echo -n $CURRENT_KEY | openssl enc -aes-256-ecb -d -a -K $KEY_HEX -nosalt -A)
ORIGINAL_CLIENT_ID=$(echo -n $CURRENT_CLIENT_ID | openssl enc -aes-256-ecb -d -a -K $KEY_HEX -nosalt -A)

printf "${NORMAL}Current key is: ${GREEN} ${CURRENT_KEY}. ${NORMAL}Will apply: ${GREEN}${ORIGINAL_KEY}${NORMAL}\n"
printf "${NORMAL}Current client_id is: ${GREEN} ${CURRENT_CLIENT_ID}. ${NORMAL}Will apply: ${GREEN}${ORIGINAL_CLIENT_ID}"

# Replace the links in the manifest
sed -i 's|'$CURRENT_KEY'|'$ORIGINAL_KEY'|g' manifest.json
sed -i 's/'$CURRENT_CLIENT_ID'/'$ORIGINAL_CLIENT_ID'/g' manifest.json

exit 0
